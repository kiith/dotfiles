import re

# read vim shiftwidth variable
def shiftwidth(vim): return int(vim.eval("&shiftwidth"))

# read vim expandtab variable
def expandtab(vim): return bool(int(vim.eval("&expandtab")))

# read vim tabstop variable
def tabstop(vim): return int(vim.eval("&tabstop"))

# get a string representing on indentation level
def indent(vim):
    return " " * shiftwidth(vim) if expandtab(vim) else "\t" * int(shiftwidth(vim) / tabstop(vim))

def meta(code):
    """Wrap any meta-code in this to make sure it includes what it needs"""
    return "from c_cpp import *\n" + code

def complete(t, opts):
    if t:
        opts = [m[len(t):] for m in opts if m.startswith(t)]
    if len(opts) == 1:
        return opts[0]
    return "(" + "|".join(opts) + ")"

def decl_scope(decl_str):
    return ''.join(decl_str.rpartition('::')[:2])

# TODO replace regex with code that will process the string, incrementing a counter
#      at '<', decrementing it at '>' and delete anything between positions
#      where the counter was 1 - i.e. toplevel parameter lists.
#      The regex approach breaks at e.g.: "shared<vector<int>, alloc> s"
# remove any template parameter lists from a string
def remove_tparams(string):
    return re.sub('<.*?>', '', string)

def func_args(params):
    return [a.strip() for a in remove_tparams(params).split(",") if not isblank(a)]

def break_line(snip, linelength, extraindent = 0, default = ""):
    textwidth = int(snip.opt("&textwidth", "80"))
    # don't break at all if textwidth is 0
    if textwidth > 0 and linelength > textwidth:
        snip.rv += snip.mkline("\n")
        snip.rv += snip.mkline(" " * extraindent)
    else:
        snip.rv += default

def rhs_members(lhs_members):
    # func_args really just splits lhs_members by ',' and removes any template args
    return ", ".join(["rhs." + a for a in func_args(lhs_members)])

def second_arg(args):
    # func_args really just splits lhs_members by ',' and removes any template args
    args = func_args(args)
    return args[1] if len(args) >= 2 else None

def setter_name(var_name):
    setter = var_name
    if setter.startswith("m_"): setter = setter[2:]
    setter = setter.strip("_")
    return "set_" + setter if setter == var_name else setter

def builder_params(lines):
    params = {}
    for line in lines.split(";"):
        (t, n) = var_type_and_name(line)
        if len(n): params[n] = t
    return params

def add_builder_setters(snip, lines):
    snip.shift()
    for name, t in builder_params(lines).items():
        const = "" if t.startswith("const ") else "const "
        snip += "Builder& {}({}{}& rhs) {{ {} = rhs; return *this; }}\n"\
                .format(setter_name(name), const, t, name)

def builder_ctor_params(lines):
    return ", ".join(builder_params(lines).keys())


# return a space if pred is true, empty string otherwise.
def space_if(pred): return " " if pred else ""

# return a colon followed by a space if pred is true, empty string otherwise.
def colon_if(pred): return ": " if pred else ""

# return a comma followed by a space if pred is true, empty string otherwise.
def comma_if(pred): return ", " if pred else ""

# is the string empty or whitespace only?
def isblank(string): return len(string) == 0 or string.isspace();

# add an initializer list to string passing all params given through ctor
def add_initializer_list(snip, ctor_params):
    snip.shift(1)
    for idx, arg in enumerate(func_args(ctor_params)):
        start = ":" if idx == 0 else ","
        # Cut off any default value (=), then take the last word of what's left
        name = arg.split("=")[0].split()[-1]
        # Move rvalue references into members
        par = "std::move(" + name + ")" if (arg.find("&&") != -1) else name 
        snip += start + " " + name + "_(" + par + ")"
    snip.reset_indent()


def guess_type(snip):
    # If in a .cpp file, this is probably a local class with name different from 
    # the filename. Otherwise use file base name by default.
    if True in map(snip.fn.endswith, (".c", ".cpp", "cxx", ".C", ".CPP", ".CXX")):
        return "Name"
    else:
        return snip.basename or "Name"

# Get owner type prefix for a definition (e.g. 'MyClass::') If owner is not
# specified, it will be guessed.
def owner(snip, owner=None):
    return ((owner if owner else guess_type(snip)) + "::") if snip.fn.endswith(".cpp") else ""

# get type and name from a variable declaration/definition.
def var_type_and_name(declaration):
    (t, _, n) = declaration.split("=")[0].strip().rpartition(" ")
    return (t, n)

def hash_code(snip, shift, align):
    types_names = [var_type_and_name(line) for line in snip.v.text.split(";")]
    hash_lines = ["hash<{}>()(rhs.{})".format(t, n) for (t, n) in types_names if t and n]
    snip.shift(shift)
    return " ^".join(["\n" + " " * align + snip.mkline(line) for line in hash_lines]).strip()

def doxy_comment(doc_line):
    doc_line = doc_line.strip()
    if doc_line.startswith("///"): return "///"
    if doc_line.startswith("//!"): return "//!"
    if doc_line.startswith("//"):  return "//"
    return " *"

def doxy_ctorline(type_name):
    if not type_name: return ""
    use_a = "bcdfghjklmnpqrstuvwz"
    a_an = "a " if type_name[0].lower() in use_a else "an "
    return "Construct {}.".format(a_an + type_name)

def gen_doxy_params(func_params, comment):
    argnames = [a.split()[-1] for a in func_args(func_params)]
    lines = []
    if argnames:
        lines.append(comment)
        maxwidth = max([len(a) for a in argnames])
        for argname in argnames:
            lines.append(comment + " @param " + argname.ljust(maxwidth) + " DOCUMENT")
    return lines

# generate doxygen @param comments from param_string and add them to snip
def add_doxy_params(snip, func_params, comment):
    for line in gen_doxy_params(func_params, comment):
        snip += line

def gen_doxy_return(func_return, comment):
    parts = func_return.split(" ")
    while parts and parts[0] in ["inline", "extern", "static", "virtual"]:
        parts = parts[1:]
    function_return = " ".join(parts)
    lines = []
    if not function_return in ["", "void"]:
        lines.append(comment)
        lines.append(comment + " @return TODO DOCUMENT")
    return lines

# generate doxygen @return comment if func_return is not void and add it to snip
def add_doxy_return(snip, func_return, comment):
    for line in gen_doxy_return(func_params, comment):
        snip += line

# generate doxygen @throw comments if func_body contains any exception mentions
def add_doxy_throws(snip, func_body, comment):
    exceptions = re.findall(r'\w*exception\b', func_body, re.IGNORECASE)
    if exceptions:
        snip += comment
        maxwidth = max([len(e) for e in exceptions])
        for e in exceptions:
            snip += comment + " @throw " + e.ljust(maxwidth) + " DOCUMENT"

def gen_doxy_end(comment):
    if comment == " *":
        return [" */"]

def add_doxy_end(snip, comment):
    if comment == " *":
        snip += " */"


### snippet context functions ###

# true if the current line contains *only* specified text and nothing else.
def emptyln(ctx, text):
    return re.match('^' + text + '$', ctx.buffer[ctx.line])

# true if the current line contains only specified text and optionally whitespace.
def blankln(ctx, text):
    return re.match('^' + text + '$', ctx.buffer[ctx.line].strip())

# true if the current line is not a comment and ends with only specified text and optionally whitespace.
def endln(ctx, text):
    line = ctx.buffer[ctx.line].strip()
    # TODO can we use  Vim's API to better determine if this is a comment?
    #      single-line check does not suffice for /* */ comments as there
    #      may be a comment line with no prefix in between
    for c in ['//', '/*']:
        if line.startswith(c):
            return False
    return line.endswith(text)

# true if not on an '#include' line
def notinclude(ctx):
    return not ctx.buffer[ctx.line].strip().startswith("#include")

def extension_in(filename, exts):
    from os.path import splitext
    return splitext(filename)[1].lower() in exts

def header_file(ctx):
    return extension_in(ctx.buffer.name, [".h", ".hpp", ".hh", ".hxx"])

def source_file(ctx):
    return extension_in(ctx.buffer.name, [".c", ".cpp", ".cc", ".cxx"])

### Common context logic for scopes ###

# Note that all of this ignores comments and will be thrown off by code in
# comments (as well as a number of other C++ weirdness: macros, digraphs, etc.)

MAX_SCAN_LINES=256
def lines_backward(y):       return range(y, max(-1, y - MAX_SCAN_LINES), -1)
def lines_forward(y, ctx):  return range(y, min(len(ctx.buffer), y + MAX_SCAN_LINES))

def find_scope_below(ctx, x, y):
    line = ctx.buffer[y][x:]
    offset = line.find('{')
    if offset >=0:                return (x + offset, y)
    if y + 1 == len(ctx.buffer): return (-1, -1)
    for l in lines_forward(y + 1, ctx):
        c = ctx.buffer[l].find('{')
        if c >= 0: return (c, l)
    return (-1, -1)


def find_scope_end(ctx, x, y):
    def process_line(xpos, ypos, depth):
        xstart = xpos
        for c in ctx.buffer[ypos][xstart:]:
            if c == '{': depth+=1
            if c == '}': depth-=1
            if depth < 0: return (depth, (xpos, ypos))
            ++xpos
        return (depth, None)

    depth = 0
    (depth, result) = process_line(x + 1, y, depth)
    if result: return result
    for l in lines_forward(y + 1, ctx):
        (depth, result) = process_line(0, l, depth)
        if result: return result
    return (-1, -1)

def find_scope_end_above(ctx):
    for l in lines_backward(ctx.line - 1):
        found = ctx.buffer[l].rfind('}')
        if found >= 0: return (found, l)
    return (-1, -1)

def find_scope_start(ctx, ex, ey):
    def process_line(xpos, ypos, depth):
        xend = xpos
        for c in reversed(ctx.buffer[ypos][:xend + 1]):
            if c == '}': depth+=1
            if c == '{': depth-=1
            if depth < 0: return (depth, (xpos, ypos))
            xpos-=1
        return (depth, None)

    depth = 0
    (depth, result) = process_line(ex - 1, ey, depth)
    if result: return result
    for l in lines_backward(ey - 1):
        (depth, result) = process_line(len(ctx.buffer[l]) - 1, l, depth)
        if result: return result
    return (-1, -1)

def find_function_signature(ctx):
    # this is grossly inadequate (for example, if the return type is templated).
    # Maybe rewrite eventually.
    # Also, obviously, nested functions (and nested types with functions)
    # will break this.
    funre = re.compile(r'^\s+\w+\s+(\w+::)?\w+\s*\(')
    for l in lines_backward(ctx.line - 1):
        found = funre.search(ctx.buffer[l])
        if found: return (found.start(0), l)
    return (-1, -1)

def find_function_rettype(ctx):
    (x, y) = find_function_signature(ctx)
    if x == -1: return None
    before_args = ctx.buffer[y].split('(')[0]
    while before_args and not before_args[-1].isspace():
        before_args = before_args[:-1]
    return before_args.strip()

### Common context logic for finding blocks above current position

def find_matching_block_above(ctx, regex_src, y):
    r = re.compile(regex_src)
    for l in lines_backward(y - 1):
        found = r.search(ctx.buffer[l])
        if found:
            (sx, sy) = find_scope_below(ctx, found.end(0), l)
            return (sx, sy, found)
    return (-1, -1, None)


def find_classstruct_above(ctx, y):
    # TODO make class name detection work for templated classes
    (sx, sy, match) = find_matching_block_above(ctx, r'(class|struct)\s*(\w+).*', y)
    return (sx, sy, match.group(2)) if sx != -1 else (-1, -1, None)

def find_switch_above(ctx):
    (sx, sy, match) = find_matching_block_above(ctx, r'switch\s*\(.+\)', ctx.line)
    return (sx, sy) if sx != -1 else (-1, -1)

def find_if_above(ctx, y):
    (sx, sy, match) = find_matching_block_above(ctx, r'if\s*\(.+\)', y)
    return (sx, sy) if sx != -1 else (-1, -1)

### Context logic for the #include snippets ###
def include_mark(ctx, idx):
    if idx >= 0 and idx < len(ctx.buffer):
        line = "".join(ctx.buffer[idx].split())
        if line.startswith('#include<'): return '<>'
        if line.startswith('#include"'): return '""'
    return ''

# use <> if: 
# a <> b <>
# a <> b
# a    b <>
# a <> b ""
# use "" if:
# a "" b ""
# a "" b
# a    b ""
# a "" b <>
def context_include_tag(ctx, prefix):
    if not blankln(ctx, prefix):
        return ''
    a = include_mark(ctx, ctx.line - 1)
    b = include_mark(ctx, ctx.line + 1)
    if a == '<>' or (a == '' and b == '<>'): return '>'
    if a == '""' or (a == '' and b == '""'): return '"'
    return '>'

### Context logic for switch case

def case_context(ctx):
    if not blankln(ctx, 'case'):
        return False
    (sx, sy) = find_switch_above(ctx)
    if sx == -1:
        return False
    (ex, ey) = find_scope_end(ctx, sx, sy)
    if ex == -1:
        return False
    def pos(x, y): return x + y * 65536
    p = pos(ctx.column, ctx.line)
    return p > pos(sx, sy) and p < pos(ex, ey)
    #return ctx.line > sy and ctx.line < ey

### Context logic for else/elif

# works with if/else and even with else if chains.
#
# does *now* work with 'if() {' with brace on the same line.
# Need to make find_if_above smarter than just line-wise.
def else_context(ctx, prefix):
    if not blankln(ctx, prefix): return False
    (ex, ey) = find_scope_end_above(ctx)
    if ex == -1: return False
    (sx, sy) = find_scope_start(ctx, ex, ey)
    if sx == -1: return False
    # finds the start of the scope of the nearest if above the found start of
    # scope, and checks if these two scopes are one and the same
    (sx2, sy2) = find_if_above(ctx, sy)
    if sx2 == -1: return False
    return (sx, sy) == (sx2, sy2) and ctx.line == ey + 1

### Context logic for functions/getting params of current function

def in_void_func(ctx):
    return find_function_rettype(ctx) == 'void'




### pre_expand/post_expand/post_jump actions

def find_or_open_cpp_buf(snip, vim):
    from os.path import splitext, isfile
    #from subprocess import call
    base = splitext(vim.current.buffer.name)[0]
    exts = [".c", ".cpp", ".cc", ".cxx"]
    # find open buffer
    for buf in vim.buffers:
        (b, ext) = splitext(buf.name)
        if base == b and ext.lower() in exts:
            return buf
    # no such buffer; find the file and open it
    for ext in exts:
        path = base + ext
        #call(['logger', path, str(isfile(path))])
        if not isfile(path):
            continue
        cmd = 'split {}'.format(path)
        vim.command(cmd)
        # get buffer for the newest opened file - works because indexing from 1
        buf = vim.buffers[len(vim.buffers)]
        if buf.name == path:
            # else we failed to open the file
            return buf
    # no such file. give up.
    return None

def find_window(name, vim):
    n = 0
    for win in vim.windows:
        if win.buffer.name == name: return n
        n += 1
    return -1



def find_class_name(snip):
    """Find name of the class/struct we're in right now, if any"""
    (y, x) = snip.snippet_start
    (sx, sy) = find_scope_start(snip, x, y)
    if sx == -1:
        return None
    (sx2, sy2, name) = find_classstruct_above(snip, sy)
    if sx2 == -1:
        return None
    return name if (sx, sy) == (sx2, sy2) else None

def post_doxy_gen(snip, vim, ret, params):
    """ Generate doxygen post-jump after full expansion of a function snippet.
    """
    prefix = indent(vim) * int(snip.snippet_start[1] / shiftwidth(vim))
    l = snip.snippet_start[0]
    snip.buffer.append(prefix + "/**" + "TODO DOCUMENT", l)
    l += 1
    comment = doxy_comment("/**")
    for line in gen_doxy_params(params, comment):
        snip.buffer.append(prefix + line, l)
        l += 1
    if ret != None:
        for line in gen_doxy_return(ret, comment):
            snip.buffer.append(prefix + line, l)
            l += 1
    if comment == " *":
        snip.buffer.append(prefix + " */", l)

def fun_hpp_post_jump(snip, vim):
    from subprocess import call
    if snip.tabstop != 0:
        return
    def t(i): return snip.tabstops[i].current_text
    # Cannot do 'throws', as the function body is inserted after the snippet
    # is finished - the text entered is not known to the snippet
    post_doxy_gen(snip, vim, t(1), t(3))
    class_name = find_class_name(snip)
    #call(['logger', "class name: ", str(class_name)])
    line1= "{} {}{}({}){}".format(t(1), class_name + "::" if class_name else "", t(2), t(3), t(4))
    # find buffer with the .cpp file, if open
    cpp_buf = find_or_open_cpp_buf(snip, vim)
    if not cpp_buf:
        return
    # found buffer with the .cpp file: append to it.
    cpp_buf.append(["", line1, "{", indent(vim), "}"])
    (line, col) = (len(cpp_buf) - 2, shiftwidth(vim))
    snip.cursor.set(line, col)

    win_n = find_window(cpp_buf.name, vim)
    if -1 == win_n:
        # no open window with the .cpp buffer: so open it
        cmd = 'split {}'.format(cpp_buf.name)
        vim.command(cmd)
        vim.current.window.cursor = (line, col)
        return
    # move focus to the window with the .cpp buffer
    cmd = 'exe {} . "wincmd w"'.format(win_n + 1)
    vim.command(cmd)
    vim.windows[win_n].cursor = (line, col)

def fun_cpp_post_jump(snip, vim, ret_idx, params_idx, expand_idx):
    if snip.tabstop != expand_idx:
        return
    def t(i): return snip.tabstops[i].current_text
    # Cannot do 'throws', as the function body is inserted after the snippet
    # is finished - the text entered is not known to the snippet
    post_doxy_gen(snip, vim, t(ret_idx) if ret_idx != None else None, t(params_idx))


### printf/scanf snippet code ###

def parse_format_string(format_string):
    """Parse a C format string into a range of format specifier strings"""
    # from http://stackoverflow.com/questions/30011379/how-can-i-parse-a-c-format-string-in-python 
    cfmt='''\
    (                                  # start of capture group 1
    %                                  # literal "%"
    (?:                                # first option
    (?:[-+0 #]{0,5})                   # optional flags
    (?:\d+|\*)?                        # width
    (?:\.(?:\d+|\*))?                  # precision
    (?:h|l|ll|w|I|I32|I64)?            # size
    [cCdiouxXeEfgGaAnpsSZ]             # type
    ) |                                # OR
    %%)                                # literal "%%"
    '''
    import re
    return re.finditer(cfmt, format_string, flags=re.X)

def create_printf_tabstops(snip, format_tabstop, target_tabstop):
    if snip.tabstop != target_tabstop:
        return

    # Determine how many format specifiers there are
    tabstop_count = 0
    for m in parse_format_string(snip.tabstops[format_tabstop].current_text):
        tabstop_count += 1
    # Generate snippet body with enough tabstops
    snippet_body = "".join([', $' + str(i + 1) for i in range(tabstop_count)])
    # Expand created snippet
    snip.expand_anon(snippet_body)

def create_scanf_tabstops(snip, format_tabstop, target_tabstop):
    if snip.tabstop != target_tabstop:
        return

    # Determine how many format specifiers there are
    snippet_body = ""
    tabstop_count = 0
    for m in parse_format_string(snip.tabstops[format_tabstop].current_text):
        tabstop_count += 1
        ref = not m.group(0).endswith("s")
        snippet_body += ", " + ("&" if ref else "") + "$" + str(tabstop_count)

    # Expand created snippet
    snip.expand_anon(snippet_body)

### array snippet code ###

def array_row(count, off):
    return "{" + ", ".join(['$' + str(i + off) for i in range(count)]) + "}"

def array_length(array_text):
    return array_text.count(",") + 1

def recurse_array_1d(snip, target_tabstop):
    if snip.tabstop != target_tabstop:
        return
    snippet_body = ", $1$2"
    gen_next = meta("recurse_array_1d(snip, 2)")
    snip.expand_anon(snippet_body, actions={"post_jump": gen_next})

def recurse_array_2d(snip, width, first = False):
    if not first and snip.tabstop != 0:
        return
    snippet_body = ",\n" + array_row(width, 1) + "$" + str(0)
    # first row will be without the comma + newline
    if first: snippet_body = snippet_body[2:]
    gen_next = meta("recurse_array_2d(snip, " + str(width ) + ")")
    snip.expand_anon(snippet_body, actions={"post_jump": gen_next})

def create_array_tabstops(snip, target_tabstop, count_str):
    if snip.tabstop != target_tabstop:
        return
    if 0 == len(count_str):
        snippet_body = "$1, $2$3"
        gen_next = meta("recurse_array_1d(snip, 3)")
        snip.expand_anon(snippet_body, actions={"post_jump": gen_next})
        return

    try:
        snippet_body = ", ".join(['$' + str(i + 1) for i in range(int(count_str))])
        # Expand created snippet
        snip.expand_anon(snippet_body)
    except ValueError:
        # don't do anything if we can't parse the count string
        return

def create_2darray_tabstops(snip, target_tabstop, height_str, width_str):
    if snip.tabstop != target_tabstop:
        return
    if 0 == len(width_str):
        array_length = "array_length(snip.tabstops[1].current_text)"
        gen_next = meta("recurse_array_2d(snip, "+ array_length + ")")
        snip.expand_anon("{$1}$0", actions={"post_jump": gen_next})
        return

    try:
        width = int(width_str)
        if 0 == len(height_str):
            recurse_array_2d(snip, width, True)
            return

        height = int(height_str)
        snippet_body = ",\n".join(array_row(width, 1 + width * i) for i in range(height))
        # Expand created snippet
        snip.expand_anon(snippet_body)
    except ValueError as e:
        call(['logger', "error while creating 2d array tabstops:", str(e)])
        # don't do anything if we can't parse the count string
        return

### switch/case handling ###
def create_switch_tabstops(snip, target_tabstop, scope):
    """Generate a new case tabstop in a switch when target_tabstop reached.
    scope specifies the name/scope of the switch type, e.g. for enum class
    types (e.g. 'case MyEnum::Value:')"""
    if snip.tabstop != target_tabstop:
        return
    break_code   = '`!p snip.rv = "break;" if not t[2].startswith("return") else ""`'
    snippet_body = 'case ' + scope + '${1}:\n\t${2}' + break_code + '\n${0}'
    gen_next = meta("create_switch_tabstops(snip, 0, '" + scope + "')")
    snip.expand_anon(snippet_body, actions={"post_jump": gen_next})


def enum_value(key_value_tabstop):
    return key_value_tabstop.current_text.partition('=')[2].strip()

def is_power_of_two(number):
    pot = 1
    for i in range(64):
        if pot == number:
            return True
        pot = pot << 1
    return False

def enum_suggest(prev_val):
    """Suggest next enum value based on previous one"""
    flag_match_1 = re.match("^1 <<\s*([0-9]|[1-5][0-9]|6[0-3])$", prev_val)
    if flag_match_1:
        return "1 << " + str(int(flag_match_1.group(1)) + 1)
    if prev_val.startswith("0x") and is_power_of_two(int(prev_val, 16)):
        result = hex(int(prev_val, 16) << 1)
        # if the previous value was zero padded, pad to same length
        return result[:2] + (max(0, len(prev_val) - len(result)) * '0') + result[2:]
    if prev_val.isdigit():
        return str(int(prev_val) + 1)
    return prev_val

### enum handling ###
def create_enum_tabstops(snip, target_tabstop, prev_val):
    if snip.tabstop != target_tabstop:
        return
    if len(prev_val):
        suggest = enum_suggest(prev_val)
        snippet_body = ',\n${1} = ${2:' + suggest + '}${0}'
        gen_next = meta("create_enum_tabstops(snip, 0, snip.tabstops[2].current_text)")
    else:
        snippet_body = ',\n${1}${0}'
        gen_next = meta("create_enum_tabstops(snip, 0, enum_value(snip.tabstops[1]))")

    snip.expand_anon(snippet_body, actions={"post_jump": gen_next})


### ctor handling ###
def create_init_list_tabstops(snip, vim, target_tabstop, ctor_params):
    if snip.tabstop != target_tabstop:
        return

    snippet_body = ""
    for idx, arg in enumerate(func_args(ctor_params)):
        snippet_body += "\n" + indent(vim)
        snippet_body += ":" if idx == 0 else ","
        # Cut off any default value (=), then take the last word of what's left
        name = arg.split("=")[0].split()[-1]
        # Move rvalue references into members
        par = "std::move(" + name + ")" if (arg.find("&&") != -1) else name
        snippet_body += " ${" + str(idx + 1) + ":" + name + "_}(" + par + ")"

    snip.expand_anon(snippet_body)


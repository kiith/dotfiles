# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

uname -a | grep x86
# 0 means yes, 1 no
IS_X86=$?
PLATFORM_PLUGINS=

if [ $IS_X86 -eq 0 ]; then
    PLATFORM_PLUGINS=fasd
else
    PLATFORM_PLUGINS=autojump
fi


# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git catimg colored-man-pages command-not-found common-aliases cp dirpersist extract git-prompt per-directory-history rsync sudo tmux ubuntu $PLATFORM_PLUGINS)

# User configuration

export PATH="~/bin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# On ARM, use autojump as z from fasd
if [ $IS_X86 -eq 1 ]; then
    alias z=j
fi

alias tt='sudo tail -f'

alias -g T='| tee'

# from common-aliases - too nagging
unalias rm
alias magit="vim -c MagitOnly"

alias nmrestart="sudo systemctl restart NetworkManager.service"
alias cmake-debug="cmake -DCMAKE_BUILD_TYPE=Debug"
alias cmake-release="cmake -DCMAKE_BUILD_TYPE=Release"
alias cmake-reldbg="cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo"

function txz() {
    if [ -n "$1" ]
    then
        tar -cv "$1" | xz -9ev > "$1.tar.gz"
    else
        echo "txz requires a parameter: file/directory to compress"
    fi
}

function txz-fast() {
    if [ -n "$1" ]
    then
        tar -cv "$1" | xz -6v > "$1.tar.gz"
    else
        echo "txz-fast requires a parameter: file/directory to compress"
    fi
}

function tgz() {
    if [ -n "$1" ]
    then
        tar -cv "$1" | gzip -9v > "$1.tar.gz"
    else
        echo "tgz requires a parameter: file/directory to compress"
    fi
}


""" FILE FORMAT """
set encoding=utf-8

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Mapping leader here as it's used in plugin mappings
let mapleader=","

" Also here to be able to change highlighting in plugin config
" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif
colorscheme desert


"""""""""" VUNDLE CONFIG START """"""""""
let offline_computer=$OFFLINE_COMPUTER
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" TODO vim workshop:
" * Plugin categories (may be separate sessions?):
"   - UI (incsearch, signify, undotree...)
"   - Text editing language extensions (surround, easy-align, commentary, exchange...)
"   - Text editing language 'fixes' (minor changes, e.g. clever-f)
"   - Major text editing language overhauls (e.g. easymotion)
"   - Autocompletion
"   - Tools integration (vimagit, ale)
"
"   Can even do this as a series of blog posts on v01d.sk
"
" Learned:
" sWb: same as ysiWb
" vS:  surround in visual mode
" F4:  switch source/header
" C-j: autocomplete
"
" TODO learn:
" gad: align C declarations
" ;:   switch (e.g. true/false)
" C-]: jump to definition of thing under cursor (using tags)
" did: delete in double quotes
" caa: change around argument
" vib: select in parens *anywhere on this line*
" ]f:  next file
" [f:  previous file
" ]n:  next merge conflict
" [n:  previous merge conflict
" ]<space>:  add [count] empty lines above
" [<space>:  add [count] empty lines below
" ]e:  move line [count] lines down
" [e:  move line [count] lines up
" cos: toggle spell checker
" cow: toggle line wrap
" =p: paste and reindent
" >p: paste and increase indent
" <p: paste and decrease indent
" [y: encode selected as C string (newlines and everything)
" ]y: decode selected from C string
" g/: search without moving cursor
" <Tab> while searching:   Go to next match of current search string
" <S-Tab> while searching: Go to previous match of current search string
"
" TODO: vim-multiple-cursors
" TODO: vim-exchange or similar!
" TODO: look at all tpope and romainl plugins
" TODO: vim-vinegar? 
" TODO: vim-expand-region? 
" TODO: vim-bbye?
" TODO: vim-fswitch or similar (cpp/h switch)
" TODO: vim-after-object or similar
" TODO: vim-zsh-path-completion?
" TODO: vim-better-whitespace?
" TODO: vim-wipeout?
" TODO: work function/struct snippets
" TODO: ListToggle for location list (list of YCM warnings/errors) handling
" TODO: Even more plugins
" TODO: ScrollColors or similar
" TODO: Ctrl-P or similar, but get it to work
" TODO: (later) EasyMotion OR vim-sneak
" TODO: C++11 and C/C++ differentiation (based on file extension) in ycm_extra_conf.global.py
if offline_computer == '1'
    Plugin expand('file://$HOME/vim-repos/Vundle.vim')
    Plugin expand('file://$HOME/vim-repos/ultisnips')
    Plugin expand('file://$HOME/vim-repos/vim-snippets')
    Plugin expand('file://$HOME/vim-repos/vimagit')
    Plugin expand('file://$HOME/vim-repos/vim-wintabs')
    " Disabled because dependencies take too much space
    " Plugin expand('file://$HOME/vim-repos/YouCompleteMe')
    Plugin expand('file://$HOME/vim-repos/vim-commentary')
    Plugin expand('file://$HOME/vim-repos/vim-sleuth')
    Plugin expand('file://$HOME/vim-repos/vim-surround')
    Plugin expand('file://$HOME/vim-repos/vim-easy-align')
    Plugin expand('file://$HOME/vim-repos/vim-repeat')
    Plugin expand('file://$HOME/vim-repos/vim-signify')
    Plugin expand('file://$HOME/vim-repos/vim-fswitch')
    Plugin expand('file://$HOME/vim-repos/switch.vim')
    Plugin expand('file://$HOME/vim-repos/ale')
    Plugin expand('file://$HOME/vim-repos/vim-mucomplete')
    Plugin expand('file://$HOME/vim-repos/vim-localvimrc')
    Plugin expand('file://$HOME/vim-repos/vim-gutentags')
    Plugin expand('file://$HOME/vim-repos/undotree')
    Plugin expand('file://$HOME/vim-repos/vim_current_word')
    Plugin expand('file://$HOME/vim-repos/targets.vim')
    Plugin expand('file://$HOME/vim-repos/vim-unimpaired')
    Plugin expand('file://$HOME/vim-repos/clever-f.vim')
    Plugin expand('file://$HOME/vim-repos/incsearch.vim')
    Plugin expand('file://$HOME/vim-repos/vim-vinegar')
else
    Plugin expand('VundleVim/Vundle.vim')
    " snippets
    Plugin expand('SirVer/ultisnips')
    Plugin expand('honza/vim-snippets')
    " magit - awesome git staging UI
    Plugin expand('jreybert/vimagit')
    " tabs in each window
    Plugin expand('zefei/vim-wintabs')
    " C/C++ error highlighting, semantic completion, etc.
    " Plugin expand('Valloric/YouCompleteMe')
    " vim verbs for commenting
    Plugin expand('tpope/vim-commentary')
    " autodetects shiftwidth and expandtab. no config needed at all.
    Plugin expand('tpope/vim-sleuth')
    " vim verbs for surrounding (in parens, quotes, tags...)
    Plugin expand('tpope/vim-surround')
    " vim verbs for alignment
    Plugin expand('junegunn/vim-easy-align')
    " '.' repeat support for vim-surround, vim-commentary and other plugins
    Plugin expand('tpope/vim-repeat')
    " See changes in gutter. Like gitgutter, but hopefully more stable.
    Plugin expand('mhinz/vim-signify')
    " Switch between source/header files.
    Plugin expand('derekwyatt/vim-fswitch')
    " Switch a piece of text between related options (e.g. true VS false, . VS -> etc.)
    Plugin expand('andrewradev/switch.vim')
    " Asynchronous Lint Engine
    Plugin expand('w0rp/ale')
    " Autocomplete using builtin vim functionality. Not as convenient as YCM but
    " much less dependencies.
    Plugin expand('lifepillar/vim-mucomplete')
    " Local per-directory vimrc delta files (.lvimrc)
    Plugin expand('embear/vim-localvimrc')
    " TODO if this works well, update dotfiles-setup.sh with a script that
    "      will clone/unpack and compile ctags into ~/bin
    Plugin expand('ludovicchabant/vim-gutentags')
    " Graphical undo tree like Gundo
    Plugin expand('mbbill/undotree')
    " Highlight all instances of word under cursor.
    Plugin expand('dominikduda/vim_current_word')
    " Additional text objects such as delimiters ('*', ',', '+', ...) and seeking
    " to next/last text object ('dinb', 'cil"' ...)
    Plugin expand('wellle/targets.vim.git')
    " Various pairwise utilities, such as for C string/XML/URL escaping, adding
    " empty lines below/above current line, exchanging lines, toggling vim
    " settings, jumping between VCS conflict markers...
    Plugin expand('tpope/vim-unimpaired')
    " f/t across lines, with target highlighting, and f/t replaces ; for search repeat
    Plugin expand('rhysd/clever-f.vim')
    " '/' search matches all results as it is being written.
    Plugin expand('haya14busa/incsearch.vim')
    " much better, faster to use netrw (file manager)
    Plugin expand('tpope/vim-vinegar')
endif

""" BAD PLUGINS
" Plugin expand('andrewradev/splitjoin.vim')
" because it has minimal C functionality and forces a particular (uncommon) 
" code style

""" /BAD_PLUGINS




call vundle#end()
filetype plugin indent on
"""""""""" VUNDLE CONFIG END   """"""""""

"""""""""" PLUGIN CONFIG START """"""""""
" ultisnips
:let g:UltiSnipsExpandTrigger="<tab>"
:let g:UltiSnipsJumpForwardTrigger="<tab>"
:let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
" /ultisnips
" wintabs
nmap gt <Plug>(wintabs_next)
nmap gT <Plug>(wintabs_previous)
nmap T <Plug>(wintabs_close)
" /wintabs
" YCM
" let g:ycm_key_list_select_completion = ['<Down>', '<Enter>']
" " 'OM' is for Shift-Enter in console Vim (works in Konsole at least)
" let g:ycm_key_list_previous_completion = ['<Up>', '<S-Enter>', 'OM']
" " leaving this at default - <C-Space> will actually work in console vim then,
" " thanks to an internal YCM hack
" " let g:ycm_key_invoke_completion = '<Right>'
" " distinguish errors from warnings
" let g:ycm_error_symbol = 'E>'
" let g:ycm_warning_symbol = 'W>'
" " disabled because breaks UltiSnips autotrigger
" let g:ycm_auto_trigger = 0 
" let g:ycm_global_ycm_extra_conf = '~/ycm_extra_conf.global.py'
" let g:ycm_echo_current_diagnostic = 1
" " populate location list with detailed YCM diagnostics
" let g:ycm_always_populate_location_list = 1
" let g:ycm_allow_changing_updatetime = 1
" let g:ycm_autoclose_preview_window_after_insertion = 1
" " max warnings/errors to list
" let g:ycm_max_diagnostics_to_display = 32
" " we set updatetime to 500, much faster than YCM's 2000
" let g:ycm_add_preview_to_completeopt = 0
" let g:ycm_disable_for_files_larger_than_kb = 2048
" 
" " F5 to refresh YCM diagnostics
" nnoremap <F5> :YcmForceCompileAndDiagnostics<CR>
" nnoremap <leader>g :YcmCompleter GoTo<CR>
" nnoremap <leader>t :YcmCompleter GetType<CR>
" nnoremap <leader>D :YcmCompleter GetDoc<CR>
" nnoremap <leader>f :YcmCompleter FixIt<CR>
" /YCM
" vim-commentary
autocmd FileType d set commentstring=//\ %s
autocmd FileType cpp set commentstring=//\ %s
autocmd FileType c set commentstring=//\ %s
" /vim-commentary
" vim-surround
" Simpler/more finger-friendly surrounding
" (Csurround acts as 'ysi' when it cannot determine what to change;
" e.g. 'swb' is the same as 'ysiwb')
nmap s <Plug>Csurround
" /vim-surround
" vim-easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" vim-repeat support for vim-easy-align
silent! call repeat#set("\<Plug>(EasyAlign)", v:count)
if !exists('g:easy_align_delimiters')
  let g:easy_align_delimiters = {}
endif
" Alignment of C/C++ variable names. The first object in the pattern - the parens `\( \*\| \)` specifies
" the spaces to align around - either ' ' (spaces) or ' *' (space followed by pointer star). The
" rest of the pattern specifies what the spaces to align around should be *followed* by.
let g:easy_align_delimiters['d'] = { 'pattern': '\( \*\| \)\ze\S\+\s*[;=]', 'left_margin': 0, 'right_margin': 0 }
"let g:easy_align_delimiters['d'] = { 'pattern': ' \(\S\+\s*[;=]\)\@=', 'left_margin':  0, 'right_margin': 0 }
" /vim-easy-align
" vim-signify
let g:signify_realtime = 0
" /vim-signify
" vim-fswitch
noremap <F4> :FSHere<CR>
" /vim-fswitch
" switch.vim
let g:switch_mapping = ";"
autocmd FileType c,cpp let g:switch_custom_definitions =
    \ [
    \   ['==', '!='],
    \   ['float', 'double'],
    \   ['uint8_t', 'uint16_t', 'uint32_t', 'uint64_t'],
    \   ['int8_t', 'int16_t', 'int32_t', 'int64_t'],
    \   ['*', '&'],
    \   ['stdin', 'stdout', 'stderr'],
    \   ['printf', 'puts'],
    \   [' < ', ' >= ', ' > ', ' <= '],
    \   ['>>', '<<'],
    \   ['max', 'min'],
    \   ['break', 'continue'],
    \   ['%d', '%u', '%ld', '%lu', '%t', '%z'],
    \   ['return 0', 'return -1'],
    \   {
    \       'else'    : 'else if',
    \       'else if' : 'else'
    \   },
    \   {
    \     '#include\s*"\(.*\)"': '#include <\1>',
    \     '#include\s*<\(.*\)>': '#include "\1"'
    \   },
    \   {
    \     '#ifdef \(.*\)':       '#if defined(\1)',
    \     '#if defined(\(.*\))': '#ifdef \1'
    \   },
    \   {
    \     '\[\(.*\)\]': '{\1}',
    \     '{\(.*\)}':   '[\1]'
    \   },
    \   {
    \     '"\(\k\+\)"': '''\1''',
    \     '''\(\k\+\)''': '"\1"'
    \   },
    \   {
    \     'while\s*(\(.*\))': 'for (;\1;)',
    \     'for\s*(.*;\(.*\);.*)': 'while (\1)'
    \   },
    \   ['shared_ptr', 'unique_ptr'],
    \   ['map', 'unordered_map'],
    \   ['set', 'unordered_set']
    \ ]

    "\   ['else', 'else if']
" /switch.vim
" ale
let g:ale_echo_msg_error_str   = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format      = '[%linter%] %s [%severity%]'
" decrease to save CPU/battery; 300-400 should be OK
let g:ale_lint_delay           = 200
" change to 'normal' and lint_on_insert_leave to true to only lint
" when leaving insert mode/when changing in normal mode.
let g:ale_lint_on_text_changed = 'always'
let g:ale_lint_on_insert_leave = 0
let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']
" /ale
" mucomplete
let g:mucomplete#enable_auto_at_startup = 1
" omni first, followed by c-n because:
" c-n gives all keywords. good for comments/non-code. But if we try to complete
" e.g. Object.$METHOD, we will get a lot of random unrelated words. Omni will
" only list members of Object. Disadvantage: if typing a variable name or a
" random word, we'll not see interesting offers until there is no match with
" any tag.
let g:mucomplete#chains = {
            \ 'default' : ['omni', 'c-n', 'path', 'keyn', 'dict', 'uspl'],
            \ }
" Avoid annoying error messages if we don't have python2
if !has("python")
  autocmd FileType python MUcompleteAutoOff
endif

set completeopt+=menuone,noselect
set shortmess+=c   " Shut off completion messages
inoremap <expr> <c-e> mucomplete#popup_exit("\<c-e>")
inoremap <expr> <c-y> mucomplete#popup_exit("\<c-y>")
inoremap <expr>  <cr> mucomplete#popup_exit("\<cr>")
imap <c-j> <plug>(MUcompleteFwd)
imap <c-k> <plug>(MUcompleteBwd)
" switch between different vim completion methods
inoremap <silent> <plug>(MUcompleteFwdKey) <right>
imap <right> <plug>(MUcompleteCycFwd)
inoremap <silent> <plug>(MUcompleteBwdKey) <left>
imap <left> <plug>(MUcompleteCycBwd)
" /mucomplete
" undotree
nnoremap <F5> :UndotreeToggle<cr>
function g:Undotree_CustomMap()
    nmap <buffer> J <plug>UndotreeGoNextState
    nmap <buffer> K <plug>UndotreeGoPreviousState
endfunc
" /undotree
" vim_current_word
hi CurrentWord ctermbg=4
hi CurrentWordTwins cterm=bold,underline
" /vim_current_word
" targets.vim
" 'd' for double quotes, 'q' for single quotes.
let g:targets_quotes = '"d ''q `'
" /targets.vim
" incsearch.vim
map /  <Plug>(incsearch-forward)\v
map ?  <Plug>(incsearch-backward)\v
map g/ <Plug>(incsearch-stay)\v
" /incsearch.vim
"""""""""" PLUGIN CONFIG END   """"""""""





" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  set undofile		" keep an undo file (undo changes after closing)
endif
set history=1000	" keep 1000 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If unset (default), this may break plugins (but it's backward
  " compatible).
  set langnoremap
endif


set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '"' . $VIMRUNTIME . '\diff"'
      let eq = '""'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

""" ALWAYS SHOW GUTTER """ 
autocmd BufEnter * sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')





""" PERFORMANCE """
set updatetime=500

""" VISUAL """
set number
set cursorline
" Search highlighting
set hlsearch
if has("gui_running")
  ""set guifont=Consolas:h10:cDEFAULT
  set guifont=Envy\ Code\ R\ for\ Powerline:h10:cDEFAULT
  set guioptions=
endif
set list
set listchars=tab:>.,trail:*,extends:#,precedes:#,nbsp:.
set relativenumber
set ttyfast " Better redrawing (as if we had a fast terminal connection)
set colorcolumn=80,88

""" WINDOW BEHAVIOUR """
set winminheight=0
set winwidth=89
set cmdheight=1 " Command line height
" Statusline
set laststatus=2                          " Last window status line; show always
set statusline=%<%f                       " termination point, filename
set statusline+=\ %h%m%r                  " help flag, modified flag, read-only flag
set statusline+=%{gutentags#statusline()} " Gutentags plugin statusline
set statusline+=%=                        " following elements will be right-aligned
set statusline+=%-18.(%l/%L,%c%V%)        " left-justified to 18 chars: line/maxline,column,virtual column
set statusline+=%-8{ALEGetStatusLine()}   " ALE plugin statusline
set statusline+=\ %P                      " space, percentage through current file
" Code folding
set foldmethod=indent
set foldenable 
set foldlevelstart=10
set foldnestmax=10
set fillchars=vert:\|,fold:\ ,diff:\
set linespace=0
set scrolloff=16
set sidescrolloff=10
set sidescroll=1
set splitright splitbelow " Open splits to the right/below the current window


""" EDITING """
" Searching
set ignorecase
set smartcase 
set incsearch
" Command-line completion
set wildmenu 
set wildmode=full
set wildignore=*.swp,*.bak,*.bkp,*.pyc,*.class
" No code wrapping
set nowrap 
" Indentation
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smartindent
" Text width
set textwidth=88
" Allow going past end of file
set virtualedit=all
" Set dir to dir of the current file
set autochdir
" Don't move to start of line on some commands (XXX investigate)
set nostartofline
" Confirm quitting
set confirm
" Undo
set undodir=~/.vimundo
set undofile
set undolevels=4096
set undoreload=16384
" Auto-read a file when changed by external program
set autoread
" Remember commands, registers, etc; don't store >160 lines of registers
set viminfo='20,\"160 
" Hide, don't close, buffers
set hidden
" Don't create backup files
set nobackup
" Don't create swap files
set noswapfile
" Autoformatting of comments
set formatoptions=cqro1

""" MAPPINGS """
" Easy insert-to-normal mode
inoremap jj <Esc>
" Disable search highlighting
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
" Less strain when using command-line
nnoremap ' :
vnoremap ' :'<,'>
" Easy window navigation
noremap <A-h> <C-w>h
noremap <A-j> <C-w>j
noremap <A-k> <C-w>k
noremap <A-l> <C-w>l
inoremap <A-h> <Esc><C-w>ha
inoremap <A-j> <Esc><C-w>ja
inoremap <A-k> <Esc><C-w>ka
inoremap <A-l> <Esc><C-w>la
" Search/replace (generic)
noremap :" :%s:::cg<Left><Left><Left><Left>
" Search/replace (full word only)
noremap :: :%s:\<\>::cg<Left><Left><Left><Left><Left><Left>
" Search/replace (symbol under cursor)
noremap <leader>; :%s:\<<C-r>=expand("<cword>")<CR>\>::gc<Left><Left><Left>

" Fast braces
inoremap {<CR> {<CR>}<C-O>O
" reStructuredText headings
nnoremap<leader>1 yyPVr=jyypVr=
nnoremap<leader>2 yyPVr-jyypVr-
nnoremap<leader>3 yyPVr^jyypVr^
nnoremap<leader>4 yyPVr"jyypVr"

" Spell checker
noremap <leader>ss :setlocal spell spellang=en_us<CR>
noremap <leader>SS :setlocal nospell<CR>

" Select function/class/scope
nnoremap <leader>F viBjokk

" Maximize window vertically
noremap <F11> <C-w>_
" Equalize window sizes
noremap <F10> <C-w>=
" Increase/decrease window height
noremap <leader>= 6<C-w>+
noremap <leader>- 6<C-w>-
" Center screen at search results
noremap N Nzz
noremap n nzz
" Format a paragraph
nnoremap Q vipgq<Esc><Esc>
" Repeat last macro
nnoremap <F2> @@
" Paste last yank (ignoring deletes)
nnoremap <C-p> "0p
vnoremap <C-p> "0p
" Y yanks to end of line 
nnoremap Y y$
" Replaced by vim-unimpaired
" " Move single line
" nnoremap <C-j> :m .+1<CR>==
" nnoremap <C-k> :m .-2<CR>==
" " Move multiple lines
" vnoremap <C-j> :m '>+1<CR>gv=gv
" vnoremap <C-k> :m '<-2<CR>gv=gv

" System clipboard copy/paste
" nnoremap <leader>p "+p
" nnoremap <leader>y "+y
" vnoremap <leader>p "+p
" vnoremap <leader>y "+y


" DIY clipboard to work on non-X11 machines
vmap <leader>y :w! ~/.vimbuffer<CR>
nmap <leader>y :.w! ~/.vimbuffer<CR>
" paste from buffer
map <leader>p :r ~/.vimbuffer<CR>

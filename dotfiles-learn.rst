Maintenance:

* ``git subtree add --prefix vim-repos/vim-snippets https://github.com/honza/vim-snippets master --squash``

   add a subtree


tmux:
<C-b>f: search all scrollback buffers

Vim:

:MagitOnly:         git staging
gt/gT/T:            next/previous/close tab
<Enter>/<S-Enter>:  next/previous YCM completion
<C-Space>:          explicitly trigger YCM completion
<F5>:               refresh YCM diagnostics
gcc:                comment out current line
gc:                 comment out target of motion (e.g. gcaB for a block) or selected text
,d:                 get detailed info on the YCM warning/error on this line
,g:                 **goto definition/declaration/header**
,t:                 **get type of current var/func**
,D:                 **get doc for current var/func**
,f:                 **FixIt under cursor**
:lopen:             see all YCM errors/warnings

Commands:

* z:                 jump to dir
* zz:                jump to dir interactively
* l:                 ls -lh
* x:                 extract
* cpv:               nice cp
* sgrep:             recursive grep
* t:                 tail -f
* tt:                sudo tail -f
* cmake-debug        cmake -DCMAKE_BUILD_TYPE=Debug
* cmake-release      cmake -DCMAKE_BUILD_TYPE=Release
* cmake-reldbg       cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo
* dud:               dir disk usage
* ff:                find file
* vim `f exp`:       open file matching `exp`
* cp file `d exp`:   move file to dir matching `exp`
* ta `session`:      tmux attach -t `session`
* git ignore file:   add file to .gitignore
* git archive-file:  archive current repo
* git effort:        effort stats on files
* git summary:       summary of repo
* ag:                sudo apt-get
* agi:               sudo apt-get install
* agud:              sudo apt-get update && sudo apt-get dist-upgrade

* fd:                find dir
* lt:                recursive ls
* lt:                ls by time
* git changelog:     generate git changelog

Bindings:

* <C-G>:             toggle global/directory history
* <Esc><Esc>:        sudo `previous command`

Operators:
* L:                 | less
* T:                 | tee
* G:                 | grep
* T:                 | head
* H:                 | tail
* LL:                2>&1 | less
* NUL                > /dev/null 2>&1

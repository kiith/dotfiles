#!/bin/bash

OFFLINE=0

echo '=== Kiith-Sa dotfiles setup/install script ==='
echo ''
echo 'This script applies changes in current directory to allow'
echo 'testing without affecting the home directory. Run from the'
echo 'home directory to install.'
echo ''
echo 'Note: This script assumes a Debian family Linux distro'
echo ''

read -p "Do you need to work without internet access? (y/N)" -n 1 -r
echo # move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
    OFFLINE=1
fi

uname -a | grep x86
# 0 means yes, 1 no
IS_X86=$?
PLATFORM_PACKAGES=

echo "packages setup"
if [ $IS_X86 -eq 0 ]; then
    if [ $OFFLINE -eq 1 ]; then
        echo "offline mode; skipping adding fasd repository"
    else
        sudo add-apt-repository ppa:aacebedo/fasd
    fi
    PLATFORM_PACKAGES=fasd
else
    PLATFORM_PACKAGES=autojump
fi

YCM_PACKAGES="cmake python-dev python3-dev"
PACKAGES="vim zsh tig imagemagick pigz git-extras curl $YCM_PACKAGES $PLATFORM_PACKAGES"

if [ $OFFLINE -eq 1 ]; then
    echo "offline mode; skipping installing packages"
    echo "packages we would install otherwise: ($PACKAGES)"
else
    sudo apt update
    sudo apt install $PACKAGES
fi


OHMYZSH=~/.oh-my-zsh
if [ ! -d "$OHMYZSH" ]; then
    echo "installing oh-my-zsh"
    # TODO offline oh-my-zsh installation from a local repo
    if [ $OFFLINE -eq 1 ]; then
        echo "offline mode; skipping oh-my-zsh installation"
    else
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    fi
fi

OFFLINE_ENVVAR_LINE="export OFFLINE_COMPUTER=1"
if [ $OFFLINE -eq 1 ]; then
    echo "adding OFFLINE_COMPUTER=1 envvar to .zshrc"
    if grep -q "$OFFLINE_ENVVAR_LINE" .zshrc; then
        echo "$OFFLINE_ENVVAR_LINE already found in .zshrc, not adding"
    else
        echo "$OFFLINE_ENVVAR_LINE" >> .zshrc
    fi

    echo "unpacking repos in vim-repos"
    cd vim-repos
    for filename in *.tar.gz
    do
        echo "    $filename"
        tar -xf $filename
    done
    cd ..
fi


VIMPKG="Vundle.vim"
if [ ! -d ".vim/bundle/$VIMPKG" ]; then
    echo "installing $VIMPKG"
    mkdir -p .vim/bundle
    cp "vim-repos/$VIMPKG.tar.gz" .vim/bundle
    cd .vim/bundle
    tar -xf "$VIMPKG.tar.gz"
    rm "$VIMPKG.tar.gz"
    cd ../..
fi

read -p "Running Vim for $VIMPKG to install packages; exit when done" -n 1 -r
vim -c ":PluginInstall"


# no YCM for offline configuration: we'd need to store all the dependencies in our repo
# (hundreds of megs).
if [ $OFFLINE -eq 0 ]; then
    # This is more likely to work on Ubuntu/Debian
    cd ~/.vim/bundle/YouCompleteMe
    # Running w/ python3 so it doesn't build YCM against python2 (vim in Ubuntu uses python3)
    python3 ./install.py --clang-completer

#   # OLD YCM INSTALL PROCEDURE - BUGGED ON E.G. UBUNTU
#   YCM_DEP_DIR=~/vim_ycm_dep
#   YCM_BUILD_DIR=~/vim_ycm_build
#   CLANG_DEST="llvm-clang"
#   CLANG_BUILD=""
#   echo "Installing YouCompleteMe dependencies to $YCM_DEP_DIR"
#   mkdir $YCM_DEP_DIR
#   cd $YCM_DEP_DIR
#
#   if [ $IS_X86 -eq 0 ]; then
#       # TODO handling for individual llvm builds (e.g. debian, different ubuntu versions)
#       CLANG_VER="4.0.0"
#       CLANG_BUILD="clang+llvm-${CLANG_VER}-x86_64-linux-gnu-ubuntu-16.10"
#       wget http://releases.llvm.org/${CLANG_VER}/${CLANG_BUILD}.tar.xz
#       tar -xf ${CLANG_BUILD}.tar.xz 
#       mv ${CLANG_BUILD} $CLANG_DEST
#       rm ${CLANG_BUILD}.tar.xz
#   else
#       # TODO arm build
#   fi
#
#   cd ~
#   mkdir "$YCM_BUILD_DIR"
#   cd "$YCM_BUILD_DIR"
#   cmake -G "Unix Makefiles" -DPATH_TO_LLVM_ROOT=${YCM_DEP_DIR}/${CLANG_BUILD} . ~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp
#   cmake --build . --target ycm_core --config Release

fi
